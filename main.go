package main

import "flag"
import "fmt"
import "log"
import "net"
import "os"
import "strconv"
import "sync"
import "time"

var (
	logger *log.Logger
)

func servePort(port int) {
	listen_address := net.JoinHostPort("", strconv.Itoa(port))
	listen, err := net.Listen("tcp", listen_address)
	if err != nil {
		logger.Printf("Failed to bind to port: %s, %s", listen_address, err.Error())
		return
	}
	defer listen.Close()

	logger.Printf("%s >> Waiting on for requests...\n", listen.Addr().String())
	for {
		conn, err := listen.Accept()
		if err != nil {
			logger.Printf("%s >> Processing incoming request failed: %s", listen.Addr().String(), err.Error())
			return
		}

		go handleRequest(conn)
	}

}

func handleRequest(conn net.Conn) {
	defer conn.Close()

	timeout := time.Duration(10 * time.Second)
	conn.SetDeadline(time.Now().Add(timeout))

	logger.Printf("%s >> Process incoming request from %s",
		conn.LocalAddr().String(),
		conn.RemoteAddr().String(),
	)

	message := fmt.Sprintf("\n==> You have reached me on %s from %s\n\n",
		conn.LocalAddr().String(),
		conn.RemoteAddr().String(),
	)
	conn.Write([]byte(message))
}

func main() {
	logger = log.New(os.Stdout, "portclaw ", log.Ltime)

	checkmkPort := flag.Int("checkmk-port", 6556, "default check_mk port")
	flag.Parse()

	var ports []int

	if len(flag.Args()) < 1 {
		ports = append(ports, *checkmkPort)
	}

	for _, rawPort := range flag.Args() {
		port, err := strconv.Atoi(rawPort)
		if err != nil {
			logger.Printf("Invalid port specified: %s", rawPort)
			continue
		}
		ports = append(ports, port)
	}
	if len(ports) < 1 {
		logger.Printf("No ports for binding found. Exiting.")
		os.Exit(1)
	}

	var wg sync.WaitGroup

	for _, port := range ports {
		wg.Add(1)
		go func(p int) {
			defer wg.Done()
			//logger.Printf("Starting server engine for port %d", p)
			servePort(p)
		}(port)
	}

	//time.Sleep(2 * time.Second)
	//logger.Printf("All handlers started. Stopping with <Ctrl+C>")

	wg.Wait()
}
